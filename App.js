import React from "react";
import { createStackNavigator } from "react-navigation";
import SplashScreen from "./src/screens/splash";
import HomeScreen from "./src/screens/homeScreen";
import AddItems from "./src/screens/addItems";
import ViewItems from "./src/screens/viewItems";
import WatchVideos from "./src/screens/watchVideos";
import Cart from './src/screens/cartScreen';
import Test from './src/screens/test';
import Notes from './src/screens/example';
import Order from './src/screens/orderConfirmation';
import SpikeBall from './src/screens/spikeBall'
import PackagesPlan from './src/screens/packagesPlan'
import SubscriptionDetail from './src/screens/Subscription'
import SubscriptionsPlan from './src/screens/subscriptionPlan'
import EventsDetails from './src/screens/eventDetails'
import Events from './src/screens/Events'
//import RootStack from "./src/routes";

const RootStack = createStackNavigator(

  {
    Events,
    EventsDetails,
    PackagesPlan,

    SubscriptionsPlan,
    SubscriptionDetail,
    Order,
    Cart,
    SpikeBall,
    Test,
    WatchVideos,
    HomeScreen,
    ViewItems,
    AddItems,
    Notes,

  },
);

export default class App extends React.Component {
  state = { isSplashOff: false };

  componentDidMount() {
    setInterval(() => {
      this.setState({ isSplashOff: true });
    }, 2000);
  }

  render() {
    if (this.state.isSplashOff) return <RootStack />;
    return <SplashScreen />;
  }
}

// import React from "react";
// import { View, Text } from "react-native";
// import { createStackNavigator } from "react-navigation";

// class HomeScreen extends React.Component {
//   render() {
//     return (
//       <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
//         <Text>Home Screen 111</Text>
//       </View>
//     );
//   }
// }

// export default createStackNavigator({
//   Home: {
//     screen: HomeScreen
//   }
// });
