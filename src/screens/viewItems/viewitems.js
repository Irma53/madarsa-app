import React from "react";
import {
  Container,
  View,
  Text,
  Item,
  Input,
  Left,
  Icon,
  Right,
  Button,
  Header,
  Body,
  Title
} from "native-base";

import {
  StyleSheet,
  WebView,
  Platform,
  Linking,
  TouchableOpacity, Image
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from "moment";

class ViewItems extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = {
      isTimePickerVisible: false,
      chosenTime: "",
      title: "",
      description: "",
      duration: "",
      url: ""
    };
  }

  render() {
    const data = this.props.navigation.state.params.getItem;
    console.log("This is the item", data);
    return (
      <View style={{ flex: 1 }}>
        <Header
          style={{ backgroundColor: "red" }}
          androidStatusBarColor="white"
        >
          <Left>
            <Icon
              name="arrow-back"
              color="black"
              onPress={() => this.props.navigation.navigate("HomeScreen")}
            />
          </Left>
          <Body>
            <Title style={styles.body}>Item Information</Title>
          </Body>
        </Header>
        <View style={styles.main}>
          <View style={styles.bottomcontainer}>
          <View style={{flexDirection:"row", flex:1, marginBottom:40, marginLeft:10}}>
          <Image
                      style={{ height: 40, width: 40  }}
                      source={{ uri: data.image }}
                    />

            <Item regular style={{ height: 50 ,marginLeft:15,width:60, flex:1}}>
              <Input
                disabled={true}
                value={data.key}
                style={{ color: "black", fontSize:25 }}
              />
            </Item>

            <Item regular style={styles.des_input}>
              <Input
                disabled={true}
                style={{ color: "black" }}
                value={data.time}
              />
            </Item>
            </View>

            <Item regular style={styles.describe}>
              <Input
                disabled={true}
                style={{ color: "black", margin:10}}
                value={data.Name}
                multiline={true}
              />
            </Item>

            

            <View
              style={{
                alignSelf: "stretch"
              }}
            >
              <TouchableOpacity
                style={styles.ybutton}
                onPress={() =>
                  Linking.openURL("https://www.youtube.com/watch?v=pEEOThZLEVs")
                }
              >
                <Icon
                  active
                  name="logo-youtube"
                  size={20}
                  style={styles.icons}
                />
                <Text style={styles.ybuttonText} uppercase={true}>
                  Youtube
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    //marginTop: 200,
    fontSize: 20
  },
  main: {
    backgroundColor: "white",
    justifyContent: "center",
      paddingTop: 50,
      marginBottom:20,
     paddingLeft: 10,
     paddingRight: 10
  },
  bottomcontainer: {
   // flex: 1,
   // paddingTop: 25,
   // paddingLeft: 30,
   // paddingRight: 30
  },
  icons: {
    color: "white",
    marginRight: 10
  },

  input: {
    color: "white"
  },
  des_input: {
    marginLeft: 40,
   // paddingBottom: 20,
    height: 40,
    width :50,
    
  },
  button: {
    //flex:1,
    marginTop: 80,
    height: 45,
    marginLeft: 50,
    width: 200,
    alignItems: "center",
    borderRadius: 50,
    justifyContent: "center",
    backgroundColor: "black"
  },

  buttonText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16
  },
  ybutton: {
    flexDirection: "row",
    marginTop:30,
    // marginRight: 10,
     marginLeft: 100,
    marginBottom: 30,
    width: 140,
    height: 45,
    borderRadius: 50,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center"
  },

  ybuttonText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16
  },
  describe: {
    //flex:1,
    paddingBottom: 20,
    marginTop: 60,
    height: 150,
    
  },
  body: {
    // alignItems: "center",
    fontSize: 20,
    color: "black",
    justifyContent: "center"
  }
});

export default ViewItems;
