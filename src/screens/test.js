// import React, { Component } from 'react'
// import {Form, Image, Button, Modal} from "semantic-ui-react"
// import { FilePond, registerPlugin  } from 'react-filepond';
// import 'filepond/dist/filepond.min.css';
// import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
// import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';
// // import { FileUpload } from "primereact/fileupload";
// registerPlugin(FilePondPluginImagePreview);
// class CustomModal extends Component {
  
//   // updateEditorials = () => {
//   //   console.log("This is the state", this.state)
//   //   this.props.updateEditorials(this.state);
//   // }
//   render() {
//     const { show, handleClose, selectedEditorial = {}, updateEditorials } = this.props;
//     console.log("This is the selected state ", selectedEditorial )
//     return (
//       <Modal
//         onClose={handleClose}
//         open={show}
//         style={{ margin: "28%", width: "100vh" }}
//         size="small"
//         closeIcon
//       >
//         <Modal.Header as="h4">
//           Update Editorial
//        </Modal.Header>
//         <Modal.Content>
//           <Form>
//             <Form.Field   >
//               <label><strong>Title</strong></label>
//               <input defaultValue={selectedEditorial.title}
//                 onChange={(e) => { this.setState({ title: e.target.value }) }}
//               />
//             </Form.Field>

//             <Form.TextArea
//               defaultValue={selectedEditorial.description}
//               onChange={(e) => { this.setState({ description: e.target.value }) }}
//             />
//             <Form.Field style={{
//               display: "flex",
//               flexDirection: "row",
//               padding: 10
//             }}>
//               <label style={{ margin: 5 }}>
//                 <strong>Cover Image</strong>
//               </label>
//               <Image
//                 src={selectedEditorial.media && selectedEditorial.media.thumb}
//                 style={{
//                   height: "25vh",
//                   width: "25vh",
//                   margin: 10
//                 }}
//               />
//               <div style={{ marginTop: 40, marginLeft:30 }}>
//                 {/* <FileUpload
//                   url="./upload"
//                   mode="basic"
//                   chooseLabel=""
//                   onUpload={(e) => { this.setState({ image: e.target.value }) }}
//                 /> */}
//                  <FilePond 
                 
//                  />
                 
//               </div>
             
//             </Form.Field>
//           </Form>
//         </Modal.Content>
//         <Modal.Actions>
//           <Button
//             style={{ margin: 10 }}
//             content="Cancel"
//             floated="right"
//             color='grey'
//             onClick={handleClose}
//           />
//           <Button
//             style={{ margin: 10 }}
//             content="Update"
//             floated="right"
//             color='red'
//             onClick={() => this.props.updateEditorials(this.state)}
//           />
//         </Modal.Actions>
//       </Modal>
//     )
//   }
// }


// export default CustomModal;