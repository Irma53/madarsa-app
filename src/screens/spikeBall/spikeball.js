import React from "react";
import { StyleSheet, TouchableOpacity, FlatList, Image } from "react-native";
import {
    Header,
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
    Card,
    CardItem,
    Text,
    Item
} from "native-base";
import LinearGradient from "react-native-linear-gradient";
import Duration from "../../components/duration";
import Slideshow from "react-native-slideshow";

class SpikeBall extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            click: true,
            refresh: false,
            duration: [
                {
                    time: "1 hour",
                    cost: "$10"
                },
                {
                    time: "1/2 Day (3 hrs)",
                    cost: "$30"
                },
                {
                    time: "Full Day (6 hrs)",
                    cost: "$60"
                }
            ]
        }
    }


    _renderItem = (item, index) => (
        <View style={{ flex: 1 }}>
            {console.log("time=", item.item)}
            {console.log("sel=", this.state.selected)}
            <Duration
                selected={item.item.time == this.state.selected}
                key={item.id}
                item={item.item}
                onSelectItem={this.itemPressed.bind(this)}
            />
        </View>
    );

    itemPressed = async item => {
        console.log(item)
        console.log("This is the state", this.state.selected);
        this.setState({ selected: item.time })
    };

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.gradeint} >
                        <Left>
                            <Icon
                                name="arrow-back"
                                style={styles.backArrow}
                            />
                        </Left>
                        <Body style={{ marginRight: 30, marginTop: 30 }}>
                            <Title style={styles.header}>SPIKE BALL</Title>
                        </Body>
                        <Right style={styles.headerRight}>
                            <Icon
                                name="search"
                                style={[styles.headerRightIcons, { marginLeft: 55 }]} />
                            <Icon
                                name="cart"
                                style={[styles.headerRightIcons, { margin: 15 }]}
                            />
                        </Right>
                    </LinearGradient>
                </View>

                <View style={styles.upperContainer}>
                    <View>
                        <Slideshow
                            dataSource={[
                                { url: "https://pushtherock.org/app/uploads/2016/05/spikeball.jpg" },
                                { url: "https://pushtherock.org/app/uploads/2016/05/spikeball.jpg" },
                                { url: "https://pushtherock.org/app/uploads/2016/05/spikeball.jpg" }
                            ]}
                            containerStyle={{ borderRadius: 30 }}
                        />

                    </View>

                    <View style={{  flex:1}}>
                        <Text style={styles.reviewOrder}>
                            Description
                        </Text>
                        <Text style={styles.itemText}>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</Text>
                    </View>
                </View>
                <View style={styles.container}>
                    <View style={{ margin: 15 }}>
                        <Text style={{ fontSize: 20 }}> Select Duration</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={this.state.duration}
                            extraData={this.state}
                            numColumns={3}
                            keyExtractor={(item, index) => index}
                            renderItem={this._renderItem}
                        />
                    </View>
                    <View style={{ marginTop: 30 }}>
                        <TouchableOpacity
                            onPress={() => {
                                alert("Order Placed");
                            }}

                        >
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.orderButton} >
                                <Text style={styles.buttonText}>
                                    ADD TO CART
                    </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    gradeint: {
        flexDirection: "row",
        height: 70,
        alignItems: "flex-end"
    },
    backArrow: {
        color: "white",
        fontSize: 22,
        marginTop: 30,
        marginLeft: 20
    },
    headerRight: {
        flex: 1,
        flexDirection: "row",
        alignItems: "baseline",
        marginTop: 30
    },
    headerRightIcons: {
        color: "white",
        fontSize: 22,
    },
    upperItem: {
        height: 40,
        backgroundColor: "white"
    },
    items: {
        height: 40,
        backgroundColor: "white",
        marginTop: 10
    },
    itemText: {
       
        color: "gray",
        fontSize: 10,
        marginLeft: 7,
       // textwrap:"wrap"

    },
    upperContainer: {
        padding: 13,
        flex: 1
        // backgroundColor: "yellow"
    },
    reviewOrder: {
        fontSize: 20,
        // fontWeight: "bold",
        paddingLeft: 8,
        marginTop: 7,
        marginBottom: 9
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: 'white',
        fontWeight: "bold"

    },
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#e5e5e5"
    },
    orderButton: {
        backgroundColor: "#00b280",
        alignItems: "center",
        justifyContent: "center",
        width: 230,
        marginHorizontal: 70,
        marginBottom: 30,
        height: 45,
        borderRadius: 40
    },
})
export default SpikeBall;