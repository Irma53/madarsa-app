import React from "react";
import { StyleSheet, TouchableOpacity, FlatList, Image } from "react-native";
import {
    Header,
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
    Card,
    CardItem,
    Text,
    Item
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

class Order extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            click: false,
            refresh: false,
            flatlistData: [
                {
                    img:
                        "https://static.turbosquid.com/Preview/001303/250/QY/_D.jpg",
                    count: 1,
                    price: 10,
                    name: "Beach Chair",
                },
                {
                    img:
                        "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 20,
                    name: "Beach Ball",
                },
                {
                    img: "https://previews.123rf.com/images/virinka/virinka1202/virinka120200118/12480629-beach-umbrella.jpg",
                    count: 1,
                    price: 30,
                    name: "Beach Umrella"
                },
                {
                    img:
                        "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 20,
                    name: "Beach Ball",
                }

            ]
        };
    }

    _incrementCount = index => {
        this.setState(prevState => {
            const { flatlistData } = prevState;
            flatlistData[index].count = ++flatlistData[index].count;
            return { flatlistData, refresh: !prevState.refresh };
        });
    };

    _decrementCount = index => {


        if (this.state.flatlistData[index].count > 1) {
            this.setState(prevState => {
                const { flatlistData } = prevState;
                flatlistData[index].count = --flatlistData[index].count;
                console.log("flatlist data", flatlistData[index].count)
                return { flatlistData, refresh: !prevState.refresh };
            });
        }
    };

    onItemDeleted = (index) => {
        // if (this.state.flatlistData[index] )
        console.log("onpress delete button", index)
        this.setState(prevState => {
            return {
                flatlistData: prevState.flatlistData.filter(Data => {
                    return Data.index !== index;
                })
            }
        })
    }

    render() {
        const { flatlistData } = this.state;
        let totalQuantity = 0;
        let totalPrice = 0;
        flatlistData.forEach((item, index) => {
            console.log("index", index)
            totalQuantity = index + 1;
            totalPrice += item.count * item.price;
            let result = totalPrice;
        })
        console.log("total quantity", totalQuantity)

        const shadowStyle = {
            shadowOpacity: 1
        }
        return (
            <View style={{ flex: 1 }}>
                <View >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.gradeint} >
                        <Left>
                            <Icon
                                name="arrow-back"
                                style={styles.backArrow}
                            />
                        </Left>
                        <Body style={{ marginRight: 30, marginTop: 30 }}>
                            <Title style={styles.header}>ORDER CONFIRMATION</Title>
                        </Body>
                        <Right style={styles.headerRight}>
                            <Icon
                                name="search"
                                style={[styles.headerRightIcons, { marginLeft: 55 }]} />
                            <Icon
                                name="cart"
                                style={[styles.headerRightIcons, { margin: 15 }]}
                            />
                        </Right>
                    </LinearGradient>
                </View>
                <View style={styles.upperContainer}>
                    <Item regular style={styles.upperItem}>
                        <Text style={styles.itemText}>Delivery Date & Time</Text>
                        <Text style={styles.itemData}>Wednesday,January 30,2019</Text>
                    </Item>
                    <Item regular style={styles.items}>
                        <Text style={styles.itemText}>Delivery Location</Text>
                        <Text style={styles.itemData}>Miami Beach,Florida</Text>
                    </Item>
                    <Item regular style={styles.items}>
                        <Text style={styles.itemText}>Payment Method</Text>
                        <Text style={styles.itemData}>Paypal</Text>
                    </Item>
                </View>
                <View style={styles.container}>
                    <View>
                        <Text style={styles.reviewOrder}>
                            Review Order
                        </Text>
                    </View>
                    <FlatList
                        data={this.state.flatlistData}
                        extraData={this.state.refresh}
                        keyExtractor={(item, index) => `${item.index}`}
                       // onItemPressed={() => this.onItemDeleted(index)}
                        renderItem={({ item, index , props}) => (
                            <View style={{
                                flex: 1, margin: 15, paddingLeft: 3

                            }}>
                                <Card style={styles.card}>
                                    <CardItem style={{
                                        borderRadius: 13,
                                        borderColor: 'black',
                                        shadowColor: 'black',
                                        shadowOffset: { width: 7, height: 7 },
                                        shadowOpacity: 1,
                                        shadowRadius: 4,
                                        elevation: 5,

                                    }}>
                                        <View>
                                            <Image
                                                style={styles.img}
                                                source={{ uri: item.img }}
                                            />
                                        </View>
                                        <View style={{ flex: 1, flexDirection: "column" }}>
                                            <View style={{  marginTop: 5, width:"100%"}}>
                                                <Text style={styles.item}> {item.name}</Text>
                                            </View>

                                            <View style={styles.countStyle}>
                                                <Icon
                                                    name="remove"
                                                    style={{ color: "gray", margin: 7 }}
                                                    onPress={() => this._decrementCount(index)}
                                                    size={27}
                                                />

                                                <Text
                                                    style={styles.countData}
                                                >
                                                    {item.count}
                                                </Text>

                                                <Icon
                                                    name="ios-add"
                                                    style={{ color: "gray", margin: 7 }}
                                                    onPress={() => this._incrementCount(index)}
                                                    size={27}
                                                />
                                            </View>
                                            <View style={{ flex: 1, marginTop: 6 }}>
                                                <Text style={{ fontSize: 15 }}>
                                                    ${item.price * item.count}
                                                </Text>
                                            </View>
                                        </View>
                                        <View>
                                            <Icon
                                                name="trash"
                                               // value={props.flatlistData}
                                                size={18}
                                                style={{ marginLeft: 40, color: "gray" }}
                                               // onPress={props.onItemPressed}
                                            />
                                        </View>
                                    </CardItem>
                                </Card>
                            </View>
                        )}
                    />
                </View>
                <View style={styles.bottomContainer}>
                    <View style={{ flexDirection: "row" }}>
                        <View style={{ flex: 1, margin: 5, flexDirection: "row" }}>
                            <Text style={{ fontSize: 14, fontWeight: "bold", marginLeft: 10 }}>Total - </Text>
                            <Text style={{ fontSize: 14, color: "gray" }}>{totalQuantity} Items</Text>
                        </View>
                        <View style={{ margin: 5 }}>
                            <Text style={{ fontSize: 12 }}> ${totalPrice} </Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <TouchableOpacity
                            onPress={() => {
                                alert("Order Placed");
                            }}
                        >
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.orderButton} >
                                <Text style={styles.buttonText}>
                                    PAY NOW
                               </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    header: {
        width: 190,
        fontSize: 16,
        color: "white",
        marginLeft: 20,
        marginRight: 70,
    },
    countStyle: {
        flex: 1, 
        flexDirection: "row",
        marginTop: 10,
        padding: 6,
        alignItems: "center"
    },
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "white"
    },
    bottomContainer: {
        backgroundColor: "white"
    },
    orderButton: {
        alignItems: "center",
        justifyContent: "center",
        width: 230,
        marginHorizontal: 70,
        marginBottom: 40,
        height: 45,
        borderRadius: 40,
        borderColor: 'black',
        shadowColor: 'black',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 1,
        shadowRadius: 4,
        elevation: 5,
    },
    item: {
        fontSize: 16
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: 'white',
    },
    upperContainer: {
        padding: 13,
        backgroundColor: "white"
    },
    gradeint: {
        flexDirection: "row",
        height: 70,
        alignItems: "flex-end"
    },
    backArrow: {
        color: "white",
        fontSize: 22,
        marginTop: 30,
        marginLeft: 20
    },
    headerRight: {
        flex: 1,
        flexDirection: "row",
        alignItems: "baseline",
        marginTop: 30
    },
    headerRightIcons: {
        color: "white",
        fontSize: 22,
    },
    upperItem: {
        height: 40,
        backgroundColor: "white"
    },
    items: {
        height: 40,
        backgroundColor: "white",
        marginTop: 10
    },
    itemText: {
        flex: 1,
        color: "gray",
        fontSize: 13,
        marginLeft: 7

    },
    itemData: {
        color: "lightgray",
        fontSize: 12
    },
    reviewOrder: {
        fontSize: 20,
        fontWeight: "bold",
        paddingLeft: 20,
        marginBottom: 8
    },
    card: {
        height: 120,
        width: "96%",
        borderRadius: 13,
        // marginTop: 10,
    },
    img: {
        height: 90,
        width: 90,
        margin: 10
    },
    countData: {
        fontSize: 20,
        //marginLeft: 8,
        //marginRight: 8,
        margin: 7,
        color: "gray"
    }

})
export default Order;






