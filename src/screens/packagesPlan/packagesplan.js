import React from "react";
import { StyleSheet, TouchableOpacity, FlatList, Image, ImageBackground } from "react-native";
import {
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
    Card,
    Text,
} from "native-base";
import LinearGradient from "react-native-linear-gradient";
import Api from "../../utils/api"

class PackagesPlan extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            click: false,
            refresh: false,
            packages: [],
            flatlistData: [
                {
                    img:
                        "https://static.turbosquid.com/Preview/001303/250/QY/_D.jpg",
                    count: 1,
                    price: 10,
                    name: "Beach Chair",
                },
                {
                    img:
                        "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 20,
                    name: "Beach Ball",
                },
                {
                    img: "https://previews.123rf.com/images/virinka/virinka1202/virinka120200118/12480629-beach-umbrella.jpg",
                    count: 1,
                    price: 30,
                    name: "Beach Umrella"
                },
                {
                    img:
                        "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 20,
                    name: "Beach Ball",
                }

            ]
        };
    }

    componentDidMount() {
        this.getPackagesById();
    }
    async getPackagesById() {
        try {
            const response = await Api.getPackagesById();
            console.log("packages plan data", response);
            this.setState({ packages: response })
            console.log("packages setstate", this.state.packages.response.Essentials[0].item_description)
        }

        catch (error) {
            console.log(error)
        }
    }

    //   async getContractorProfile() {
    //     try {
    //       const { data } = await services.auth.getContractorProfile(
    //         this.props.match.params.id
    //       );
    //       console.log("Okay-data2:", data);
    //       this.setState({ contractor: data });
    //     } catch (error) {
    //       console.log(error);
    //       this.notify(null, error.data.message, null);
    //     }
    //   }


    render() {
        return (
            <View style={{ flex: 1 }}>
                {/* <ImageBackground source={{ uri: "https://image.freepik.com/free-vector/beach-tree-palms_18591-722.jpg" }} style={{ width: '100%', height: '100%' }}> */}
                <View >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.gradeint} >
                        <Left>
                            <Icon
                                name="arrow-back"
                                style={styles.backArrow}
                            />
                        </Left>
                        <Body style={{ marginRight: 30, marginTop: 30 }}>
                            <Title style={styles.header}>PACKAGES PLAN</Title>
                        </Body>
                        <Right style={styles.headerRight}>
                            <Icon
                                name="search"
                                style={[styles.headerRightIcons, { marginLeft: 55 }]} />
                            <Icon
                                name="cart"
                                style={[styles.headerRightIcons, { margin: 15 }]}
                            />
                        </Right>
                    </LinearGradient>
                </View>

                {(this.state.packages && this.state.packages.response && this.state.packages.response.package).map(function (item, index) {

                    return (
                        <View style={styles.upperContainer}>
                            <Image
                                style={styles.imageStyle}
                                source={{ uri: "https://previews.123rf.com/images/paha_l/paha_l1302/paha_l130201418/17676171-volleyball-net-volleyball-on-beach-and-palm-trees-focus-on-ball.jpg" }}
                            />
                            <View style={{ position: 'absolute', top: 55, left: 0, right: "35%", bottom: -5, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: "white", fontWeight: "bold", fontSize: 20 }}>{item.package_name}</Text>
                            </View>
                            <View style={styles.imageCard}>
                                <View style={{ flex: 1, flexDirection: "column", margin: 10 }}>
                                    <Text style={styles.imageCardTxt}>$20</Text>
                                    <Text style={styles.secondCardTxt}> For 1hr </Text>
                                </View>
                                <View style={{ flex: 1, flexDirection: "column", margin: 10 }}>
                                    <Text style={styles.imageCardTxt}>$20</Text>
                                    <Text style={styles.secondCardTxt}>For 3hr</Text>
                                </View>
                                <View style={{ flex: 1, flexDirection: "column", margin: 10 }}>
                                    <Text style={styles.imageCardTxt}>$20</Text>
                                    <Text style={styles.secondCardTxt}>For 6hr</Text>
                                </View>
                                <View style={{ flex: 1, flexDirection: "column", margin: 10 }}>
                                    <Text style={styles.imageCardTxt}>$20</Text>
                                    <Text style={styles.secondCardTxt}>For 12hr</Text>
                                </View>
                            </View>
                        </View>

                    );

                   }
                )
            }



                <View style={styles.container}>
                    <FlatList
                        data={this.state.packages.response && this.state.packages.response.Essentials}
                        extraData={this.state.refresh}
                        // keyExtractor={(item, index) => item.id}
                        keyExtractor={(item, index) => `${item.index}`}
                        renderItem={({ item }) => (
                            <View style={{ flex: 1, margin: 5, justifyContent: "center", alignItems: "center" }}>
                                <Card style={styles.card}>
                                    <View style={{ flex: 1, flexDirection: "row" }}>
                                        <Image
                                            style={styles.img}
                                            source={{ uri: item.item_image }}
                                        />
                                        <View style={{ flex: 1, flexDirection: "row", marginVertical: 30 }}>
                                            <Text style={styles.item}>
                                                {/* {item.name} */}
                                                {item.item_name}
                                            </Text>
                                            <Text style={styles.countData} >
                                                {/* {item.count} */}
                                                {item.item_qty}
                                            </Text>
                                        </View>
                                    </View>
                                </Card>
                            </View>
                        )}
                    />
                </View>
                <View style={styles.bottomContainer}>
                    <View style={{ marginTop: 5 }}>
                        <TouchableOpacity
                            onPress={() => {
                                alert("Order Placed");
                            }} >
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.orderButton} >
                                <Text style={styles.buttonText}>
                                    ORDER
                               </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* </ImageBackground> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    header: {
        width: 190,
        fontSize: 16,
        color: "white",
        marginLeft: 20,
        marginRight: 70,
    },
    countStyle: {
        flex: 1,
        flexDirection: "row",
        marginTop: 10,
        padding: 6,
        alignItems: "center"
    },
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "white"
    },
    bottomContainer: {
        backgroundColor: "white"
    },
    orderButton: {
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 40,
        marginBottom: 20,
        marginTop: 10,
        borderRadius: 40,
        borderColor: 'black',
        width: 280,
        shadowColor: 'black',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 1,
        shadowRadius: 4,
        elevation: 5,
    },
    item: {
        flex: 1,
        fontSize: 14,
        // marginTop: 20,
        fontWeight: "bold"
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: 'white',
    },
    upperContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        padding: 5,
        backgroundColor: "white",
    },
    gradeint: {
        flexDirection: "row",
        height: 70,
        alignItems: "flex-end"
    },
    backArrow: {
        color: "white",
        fontSize: 22,
        marginTop: 30,
        marginLeft: 20
    },
    headerRight: {
        flex: 1,
        flexDirection: "row",
        alignItems: "baseline",
        marginTop: 30
    },
    headerRightIcons: {
        color: "white",
        fontSize: 22,
    },
    upperItem: {
        height: 40,
        backgroundColor: "white"
    },
    items: {
        height: 40,
        backgroundColor: "white",
        marginTop: 10
    },
    itemText: {
        flex: 1,
        color: "gray",
        fontSize: 13,
        marginLeft: 7
    },
    itemData: {
        color: "lightgray",
        fontSize: 12
    },
    reviewOrder: {
        fontSize: 20,
        fontWeight: "bold",
        paddingLeft: 20,
        marginBottom: 8
    },
    card: {
        height: 80,
        width: "92%",
        borderRadius: 13,
        // backgroundColor:"lightgray"
    },
    img: {
        height: 50,
        width: 50,
        margin: 15
    },
    countData: {
        fontSize: 15,
        color: "black",
        marginHorizontal: 20,
        fontWeight: "bold"
    },
    imageStyle: {
        width: "90%",
        height: "70%",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        marginTop: 15
    },
    imageCard: {
        backgroundColor: "#3FE0D0",
        flexDirection: "row",
        marginLeft: 17,
        marginRight: 17,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    imageCardTxt: {
        fontSize: 18,
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    secondCardTxt: {
        fontSize: 13,
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    }


})
export default PackagesPlan;






