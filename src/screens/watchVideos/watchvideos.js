import React from 'react';
import { StyleSheet, FlatList, TouchableOpacity, Image } from 'react-native';
import { View, Header, Body, Text, Title, Left, Icon, Card, CardItem } from 'native-base';
import { SearchBar } from 'react-native-elements';

class WatchVideos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flatlistData: [
        {
          img: "https://www.samaa.tv/wp-content/uploads/2018/12/tariq-jameel-640x366.jpg"
        },
        {
          img: "https://www.samaa.tv/wp-content/uploads/2018/12/tariq-jameel-640x366.jpg"
        },
        {
          img: "https://www.samaa.tv/wp-content/uploads/2018/12/tariq-jameel-640x366.jpg"
        },
        {
          img: "https://www.samaa.tv/wp-content/uploads/2018/12/tariq-jameel-640x366.jpg"
        },
        {
          img: "https://www.samaa.tv/wp-content/uploads/2018/12/tariq-jameel-640x366.jpg"
        },
        {
          img: "https://www.samaa.tv/wp-content/uploads/2018/12/tariq-jameel-640x366.jpg"
        }
      ]
    }


  }
  static navigationOptions = {
    header: null
  };

  FlatListItemSeparator = () => {
    return (
      <View style={{ height: 1, width: "100%", backgroundColor: "#607D8B" }} />
    );
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          style={{ backgroundColor: "white" }}
          androidStatusBarColor="white"
        >
          <Left>

            <Icon
              name="arrow-back"
              color="black"
              onPress={() => this.props.navigation.navigate("SplashScreen")}
            />

          </Left>

          <Body>
            <Title style={styles.header}>Watch Bayan</Title>
          </Body>


        </Header>

        <View>
          <SearchBar
            lightTheme
            searchIcon={{ size: 15, marginLeft: 125, color: "black" }}
            placeholder="Search"
            platform="default"
            inputStyle={{ fontSize: 13, color: "black" }}
            containerStyle={{ backgroundColor: "white", borderRadius: 5 }}

          />
        </View>

        <View style={styles.container}>
          <FlatList

            data={this.state.flatlistData}

            keyExtractor={(item, index) => item.img}
            numColumns={2}
            renderItem={({ item }) => (




              <View style={{ flex: 1, padding: 8 }}>
                <Card transparent>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("HomeScreen");
                    }}
                  >
                    <CardItem cardBody>
                      <Image
                        style={{ height: 100, width: "90%", flex: 1 }}
                        source={{ uri: item.img }}
                      />
                      <Icon
                        name="play"


                        style={{ position: "absolute", top: 55, left: 75, color: "white" }}

                      />

                    </CardItem>
                    <CardItem
                      style={{
                        height: 50,
                        backgroundColor: "yellow",

                      }}
                    >
                      <Body
                        style={{
                          alignItems: "center",
                          justifyContent: "center",
                          flexDirection: "column"
                        }}
                      >
                        <View
                          style={{

                            paddingTop: 5,
                            paddingBottom: 5,
                            paddingLeft: 10,
                            paddingRight: 10,
                            position: "absolute",

                          }}
                        >
                          <Text
                            style={{
                              color: "black",
                              fontSize: 10,
                              fontWeight: "bold"
                            }}
                          >
                            Taqwa Hasil kerne ka tareeqa - Mufti Taqi Usmani Sahab
          </Text>
                        </View>


                      </Body>
                    </CardItem>
                  </TouchableOpacity>
                </Card>
              </View>
            )}
          />
        </View>
      </View>


    );
  }
}
const styles = StyleSheet.create({
  header: {
    alignItems: "center",
    fontSize: 20,
    color: "black",
    justifyContent: "center"
  },
})

export default WatchVideos;