import React, { Component } from "react";
import {
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Image,
  Alert
} from "react-native";
import { SearchBar} from "react-native-elements";
import {
  Header,
  Body,
  Text,
  View,
  Title,
  Left,
  Right,
  Button,
  Icon,
  Thumbnail
} from "native-base";


class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      originalFlatlist: [
        {
          key: "TITLE",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",
          time: "2:30"
        },
        {
          key: "ABC",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "3:30"
        },
        {
          key: "HELLO",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "4:30"
        },
        {
          key: "HI",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "2:30"
        },
        {
          key: "BYE",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "2:20"
        },
        {
          key: "WORLD",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "4:00"
        },
        {
          key: "TOUR",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",

          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",
          time: "1:30"
        },
        {
          key: "HEY",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",

          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",
          time: "3:00"
        },
        {
          key: "GOOD",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",

          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",
          time: "2:30"
        },
        {
          key: "EXCELLENT",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",

          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",
          time: "2:30"
        }
      ],
      FlatListItems: [
        {
          key: "TITLE",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",
          time: "2:30"
        },
        {
          key: "ABC",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "3:30"
        },
        {
          key: "HELLO",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "4:30"
        },
        {
          key: "HI",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "2:30"
        },
        {
          key: "BYE",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "2:20"
        },
        {
          key: "WORLD",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "4:00"
        },
        {
          key: "TOUR",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "1:30"
        },
        {
          key: "HEY",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "3:00"
        },
        {
          key: "GOOD",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "2:30"
        },
        {
          key: "EXCELLENT",
          Name:
            "What is the FlatList component? It’s an easy way to make an efficient scrolling list of data. ",
          image:
            "https://www.pngarts.com/files/3/Avatar-PNG-Download-Image.png",

          time: "2:30"
        }
      ]
    };
  }

  FlatListItemSeparator = () => {
    return (
      <View style={{ height: 1, width: "100%", backgroundColor: "#607D8B" }} />
    );
  };

  searchFilterFunction = text => {
    if (text) {
      const newData = this.state.FlatListItems.filter(item => {
        const itemData = item.key.toLowerCase();
        const textData = text.toLowerCase();
        return itemData.indexOf(textData) > -1;
      });
      this.setState({
        originalFlatlist: newData
      });
    } else {
      this.setState({
        originalFlatlist: this.state.FlatListItems
      });
    }
  };

  static navigationOptions = {
    header: null
  };
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          style={{ backgroundColor: "white" }}
          androidStatusBarColor="white"
        >
          <Left>
            {/* <Button onPress={() => this.props.navigation.pop()} 
            backgroundColor="black"
           > */}
            <Icon
              name="arrow-back"
              color="black"
              onPress={() => this.props.navigation.navigate("SplashScreen")}
            />
            {/* </Button> */}
          </Left>

          <Body>
            <Title style={styles.header}>Header</Title>
          </Body>

          <Right>
            <Icon
              name="md-add"
              color="black"
              style={{ fontSize: 30 }}
              onPress={() => this.props.navigation.navigate("AddItems")}
            />
          </Right>
        </Header>

        <View>
          <SearchBar
            lightTheme
            searchIcon={{ size: 15, marginLeft: 125, color: "black" }}
            placeholder="Search"
            platform="default"
            // inputContainerStyle={{Color: "white" }}
            inputStyle={{ fontSize: 13, color: "black" }}
            containerStyle={{ backgroundColor: "white", borderRadius: 5 }}
            onChangeText={text => this.searchFilterFunction(text)}
          />
        </View>

        <View style={styles.container}>
          <FlatList
            data={this.state.originalFlatlist}
            ItemSeparatorComponent={this.FlatListItemSeparator}
            renderItem={({ item }) => (
              // <ListItem
              //   roundAvatar
              //   title={`${item.key} ${item.time}`}
              //   subtitle={item.Name}
              //   avatar={{ uri: item.image }}
              // />

              <TouchableOpacity
                onPress={() => {
                  console.log("This is the item",item);
                  this.props.navigation.navigate("ViewItems",{ getItem : item});
                }}
              >
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View>
                    <Image
                      style={{ height: 50, width: 50, margin: 20 }}
                      source={{ uri: item.image }}
                    />
                  </View>

                  <View style={{ flex: 1, flexDirection: "column" }}>
                    <View style={{ flex: 1, marginTop: 20 }}>
                      <Text style={styles.item}> {item.key} </Text>
                    </View>

                    <View style={{ flex: 1, marginTop: 5 }}>
                      <Text style={{ fontSize: 9 }}> {item.Name} </Text>
                    </View>
                  </View>

                  <View style={{ flexDirection: "column" }}>
                    <View
                      style={{ flex: 1, alignItems: "center", marginTop: 30 }}
                    >
                      <Icon
                        name="ios-arrow-forward"
                        size={18}
                        color="#a9a9a9"
                        style={{ marginLeft: 60 }}
                      />
                    </View>
                    <View>
                      <Text style={{ fontSize: 11, marginLeft: 80 }}>
                       
                        {item.time}
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    // alignItems: "center",
    fontSize: 20,
    color: "black",
    marginLeft: 70,
    justifyContent: "center"
  },

  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#e5e5e5"
  },
  headerText: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    fontWeight: "bold"
  },
  item: {
    // padding: 10,
    fontSize: 15
    // height: 45,
  }
});
export default HomeScreen;
