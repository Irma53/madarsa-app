import React from "react";
import { StyleSheet, FlatList, Image, TouchableOpacity } from "react-native";
import {
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
    Text,
    Card,
    Button
} from "native-base";
import Api from "../../utils/api"
import LinearGradient from "react-native-linear-gradient";
class Events extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props)
        this.state = {
            refresh: false,
            response: [],
            events: [],
        }
    }

    renderSeparator = () => (
        <View
            style={{
                backgroundColor: "#d3d3d3",
                height: 1
            }}
        />
    );
    componentDidMount() {
        this.getEvents();
    }

    async getEvents() {
        try {
            const response = await Api.getEvents();
            console.log("Events plan data", response);
            this.setState({ events: response });
            console.log("events setstate", this.state.events)
        }
        catch (error) {
            console.log(error)
        }
    }
    nextscreen = (item) => {
        console.log("next screen data")
        this.props.navigation.navigate("EventsDetails", { info: item });
    }
    render() {
        return (
            <View style={{ flex: 1 }}>
                {/* <ImageBackground source={{ uri: "https://image.freepik.com/free-vector/beach-tree-palms_18591-722.jpg" }} style={{ width: '100%', height: '100%' }}> */}
                {/* <LinearGradient/> */}
                <View >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.gradeint} >
                        <Left>
                            <Button onPress={() => this.props.navigation.pop()} transparent>
                                <Icon name="ios-arrow-back" style={styles.backArrow} />
                            </Button>
                        </Left>
                        <Body style={{ marginRight: 30, marginTop: 30 }}>
                            <Title style={styles.header}>EVENTS</Title>
                        </Body>
                        <Right style={styles.headerRight}>
                            <Icon
                                name="search"
                                style={[styles.headerRightIcons, { marginLeft: 55 }]} />
                            <Icon
                                name="cart"
                                style={[styles.headerRightIcons, { margin: 15 }]}
                            />
                        </Right>
                    </LinearGradient>
                </View>
                <View style={{
                    flex: 1,
                    justifyContent: "center",
                    backgroundColor: "white"
                }}>
                    <FlatList
                        data={this.state.events.response}
                        extraData={this.state.refresh}
                        keyExtractor={(item, index) => `${item.index}`}
                        ItemSeparatorComponent={this.renderSeparator}
                        renderItem={({ item }) => (
                            <View style={styles.upperContainer}>

                                <Card style={styles.card}>
                                    <TouchableOpacity
                                        style={{ flex: 1 }}
                                        onPress={() => { this.nextscreen(item) }}
                                    >
                                        <View style={{ flexDirection: "column" }}>

                                            <View
                                                style={{
                                                    flex: 1, backgroundColor: "white", borderTopLeftRadius: 4,
                                                    borderTopRightRadius: 4, margin: 15
                                                }}
                                            >
                                                <Text style={{ color: "black", fontWeight: "bold", fontSize: 15 }}>{item.title}</Text>
                                            </View>
                                            <Image
                                                style={styles.imageStyle}
                                                source={{ uri: item.image }}
                                            />

                                            <View style={styles.imageCard}>
                                                <View style={{ flex: 1, flexDirection: "row", margin: 10, alignItems: "baseline" }}>
                                                    <Icon name='location' type="EvilIcons" style={{ color: 'white', fontSize: 20 }} />
                                                    <Text style={styles.imageCardTxt}>{item.location}</Text>
                                                </View>
                                                <View style={{ flex: 1, margin: 10 }}>
                                                    <Text style={styles.imageCardTxt}>{item.created_at}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                </Card>
                            </View>
                        )}
                    />
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    header: {
        width: 190,
        fontSize: 16,
        color: "white",
        marginLeft: 20,
        marginRight: 70,
    },

    upperContainer: {
        flex: 1,
        padding: 5,
        justifyContent: "center",
        alignItems: "center",
    },
    gradeint: {
        flexDirection: "row",
        height: 70,
        alignItems: "flex-end"
    },
    backArrow: {
        color: "white",
        fontSize: 22,
        marginTop: 30,
        marginLeft: 20
    },
    headerRight: {
        flex: 1,
        flexDirection: "row",
        alignItems: "baseline",
        marginTop: 30
    },
    headerRightIcons: {
        color: "white",
        fontSize: 22,
    },

    card: {
        flex: 1,
        width: "92%",
        borderRadius: 13,
        margin: 20
    },

    imageStyle: {
        height: 140,
        flex: 1
    },
    imageCard: {
        flex: 1,
        backgroundColor: "#89CFF0",
        flexDirection: "row",
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    imageCardTxt: {
        fontSize: 12,
        color: "white"
    },
})
export default Events;






