import React from "react";
import { StyleSheet, TouchableOpacity, FlatList, Image, ImageBackground } from "react-native";
import {
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
    Card,
    Text,
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

class SubscriptionDetail extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            click: false,
            refresh: false,
            flatlistData: [
                {
                    img:
                        "https://static.turbosquid.com/Preview/001303/250/QY/_D.jpg",
                    count: 1,
                    price: 10,
                    name: "Beach Chair",
                },
                {
                    img:
                        "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 20,
                    name: "Beach Ball",
                },
                {
                    img: "https://previews.123rf.com/images/virinka/virinka1202/virinka120200118/12480629-beach-umbrella.jpg",
                    count: 1,
                    price: 30,
                    name: "Beach Umrella"
                },
                {
                    img:
                        "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 20,
                    name: "Beach Ball",
                }

            ]
        };
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <View >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.gradeint} >
                        <Left>
                            <Icon
                                name="arrow-back"
                                style={styles.backArrow}
                            />
                        </Left>
                        <Body style={{ marginRight: 30, marginTop: 30 }}>
                            <Title style={styles.header}>FOR 1 PERSON</Title>
                        </Body>
                        <Right style={styles.headerRight}>
                            <Icon
                                name="search"
                                style={[styles.headerRightIcons, { marginLeft: 55 }]} />
                            <Icon
                                name="cart"
                                style={[styles.headerRightIcons, { margin: 15 }]}
                            />
                        </Right>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    {/* <ImageBackground source={{ uri: "https://image.freepik.com/free-vector/beach-tree-palms_18591-722.jpg" }} style={{ width: '100%', height: '100%' }}> */}
                    <View style={styles.uppercontainer}>
                        <Card style={styles.upperCard}>
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <Text style={styles.item}>25 Days Left</Text>
                                <Text style={styles.secondItemData} >
                                    Ending At 2:00 PM
                                         </Text>
                            </View>
                        </Card>
                    </View>
                    <View style={styles.lowercontainer}>
                        <View style={styles.secondCardView}>
                            <Card style={styles.lowerCard}>
                                <View style={styles.gameView}>
                                    <Icon
                                        name="basketball"
                                        style={{ color: "white" }}
                                    />
                                    <Text style={styles.gameitem}>GAMES</Text>
                                </View>
                            </Card>
                            <TouchableOpacity
                                onPress={() => {
                                    alert(" 4/4 Selected ");
                                }}
                                style={styles.gameTouchable}
                            >
                                <Text style={styles.buttonText}>
                                    4/4 Selected </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.essentialView}>
                            <Card style={styles.secondlowerCard}>
                                <View style={{ flex: 1, alignItems: "center", justifyContent: "center", marginVertical: 20 }}>
                                    <Icon
                                        name="ios-umbrella"
                                        style={{ color: "white" }}
                                    />
                                    <Text style={styles.gameitem}>ESSENTIALS</Text>
                                </View>
                            </Card>
                            <TouchableOpacity
                                onPress={() => {
                                    alert("1/3 items left");
                                }}
                                style={styles.essentialTouchable}
                            >
                                <Text style={styles.secondButtonText}>
                                    1/3 items left </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <FlatList
                        data={this.state.flatlistData}
                        extraData={this.state.refresh}
                        keyExtractor={(item, index) => `${item.index}`}
                        renderItem={({ item, index, props }) => (
                            <View style={{ flex: 1, margin: 5, justifyContent: "center", alignItems: "center" }}>
                                <Card style={styles.card}>
                                    <View style={{ flex: 1, flexDirection: "column" }}>
                                        <View style={{ flex: 1, flexDirection: "row" }}>
                                            <Image
                                                style={styles.img}
                                                source={{ uri: item.img }}
                                            />
                                            <View style={{ flex: 1, flexDirection: "column" }}>
                                                <Text style={{ fontSize: 14, margin: 5 }}>{item.name}</Text>
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        alert("1 Item Added");
                                                    }}
                                                    style={styles.itemTouchable}
                                                >
                                                    <Text style={styles.itemAdded} >
                                                        1 Added </Text>
                                                </TouchableOpacity>

                                            </View>
                                            <View style={styles.buttonView}>
                                                <Icon
                                                    name="trash"
                                                    style={{ color: "gray", fontSize: 25, margin: 5 }}
                                                    onPress={() => {
                                                        alert("Item Deleted");
                                                    }}
                                                />
                                                <TouchableOpacity
                                                    onPress={() => {
                                                        alert("ADD TO BAG");
                                                    }}
                                                >
                                                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.orderButton} >
                                                        <Text style={styles.buttonText}>
                                                            ADD TO BAG
                                                         </Text>
                                                    </LinearGradient>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <Text style={{ fontSize: 13, color: "grey", margin: 5 }}>
                                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                                </Text>
                                        </View>
                                    </View>
                                </Card>
                            </View>
                        )}
                    />
                    {/* </ImageBackground> */}
                </View>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    header: {
        width: 190,
        fontSize: 16,
        color: "white",
        marginLeft: 20,
        marginRight: 70,
    },
    countStyle: {
        flex: 1,
        flexDirection: "row",
        marginTop: 10,
        padding: 6,
        alignItems: "center"
    },
    container: {
        flex: 1,
        justifyContent: "center",
    },
    uppercontainer: {
        margin: 10,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'transparent'
    },
    lowercontainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    bottomContainer: {
        backgroundColor: "white"
    },
    orderButton: {
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 5,
        borderRadius: 40,
        borderColor: 'black',
        width: 100,
        shadowColor: 'black',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 1,
        shadowRadius: 4,
        elevation: 5,
    },
    item: {
        flex: 1,
        fontSize: 13,
        margin: 10,
        color: "grey"
    },
    gameitem: {
        flex: 1,
        fontSize: 12,
        margin: 4,
        color: "white"
    },
    buttonText: {
        fontSize: 10,
        textAlign: 'center',
        margin: 9,
        color: 'lightgray',
        fontWeight: "bold"
    },
    secondButtonText: {
        fontSize: 10,
        textAlign: 'center',
        margin: 9,
        color: 'grey',
    },
    upperContainer: {
        padding: 13,
        backgroundColor: "white"
    },
    gradeint: {
        flexDirection: "row",
        height: 70,
        alignItems: "flex-end"
    },
    backArrow: {
        color: "white",
        fontSize: 22,
        marginTop: 30,
        marginLeft: 20
    },
    headerRight: {
        flex: 1,
        flexDirection: "row",
        alignItems: "baseline",
        marginTop: 30
    },
    headerRightIcons: {
        color: "white",
        fontSize: 22,
    },
    upperItem: {
        height: 40,
        backgroundColor: "white"
    },
    items: {
        height: 40,
        backgroundColor: "white",
        marginTop: 10
    },
    itemText: {
        flex: 1,
        color: "gray",
        fontSize: 13,
        marginLeft: 7
    },
    itemData: {
        color: "lightgray",
        fontSize: 12
    },
    reviewOrder: {
        fontSize: 20,
        fontWeight: "bold",
        paddingLeft: 20,
        marginBottom: 8
    },
    card: {
        padding: 10,
        borderRadius: 13,
        flex: 1
    },
    upperCard: {
        height: 40,
        width: "95%",
        borderRadius: 13,
        backgroundColor: "white",
        margin: 5,
        justifyContent: "center",
        alignItems: "center",

    },
    secondlowerCard: {
        height: 90,
        width: "90%",
        borderRadius: 7,
        backgroundColor: "#FFBF00",
        alignItems: "center",
        justifyContent: "center"
    },
    lowerCard: {
        height: 90,
        width: "90%",
        borderRadius: 7,
        backgroundColor: "#7EF9FF",
        alignItems: "center",
        justifyContent: "center"
    },
    img: {
        height: 45,
        width: 45,
        margin: 10
    },
    secondItemData: {
        margin: 10,
        fontSize: 13,
        color: "grey"
    },
    secondCardView: {
        justifyContent: "center",
        alignItems: "center",
        height: 140,
        width: "45%",
        flexDirection: "column"
    },
    gameView: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginVertical: 20
    },
    gameTouchable: {
        backgroundColor: "grey",
        borderRadius: 25,
        height: 30,
        width: 100
    },
    essentialView: {
        justifyContent: "center",
        alignItems: "center",
        height: 140,
        width: "45%",
        flexDirection: "column"
    },
    essentialTouchable: {
        backgroundColor: "lightgray",
        borderRadius: 25,
        height: 30,
        width: 100
    },
    itemTouchable: {
        backgroundColor: "lightgray",
        borderRadius: 25,
        width: 50,
        height: 20
    },
    itemAdded: {
        textAlign: "center",
        fontSize: 7,
        margin: 5,
        color: "grey",
        fontWeight: "bold"
    },
    buttonView: {
        margin: 20,
        flex: 1,
        flexDirection: "row",
        margin: 10
    }


})
export default SubscriptionDetail;






