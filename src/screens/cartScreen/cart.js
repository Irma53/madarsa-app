import React from "react";
import { StyleSheet, TouchableOpacity, FlatList, Image } from "react-native";
import {
    Header,
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
    Card,
    CardItem,
    Text,
    Item
} from "native-base";
import LinearGradient from "react-native-linear-gradient";
import Duration from "../../components/duration";

class Cart extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            selected: false,
            click: true,
            refresh: false,
            duration: [
                {
                    time: "1 hour",
                    cost: "$10"
                },
                {
                    time: "1/2 Day (3 hrs)",
                    cost: "$30"
                },
                {
                    time: "Full Day (6 hrs)",
                    cost: "$60"
                }
            ],

            images: [
                {
                    img:
                        "https://static.turbosquid.com/Preview/001303/250/QY/_D.jpg",
                    count: 1,
                    price: 10,
                    name: "Beach Chair"
                },
                {
                    img:
                        "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 20,
                    name: "Beach Ball"
                },
                {
                    img: "https://previews.123rf.com/images/virinka/virinka1202/virinka120200118/12480629-beach-umbrella.jpg",
                    count: 1,
                    price: 30,
                    name: "Beach Umrella"
                },
                {
                    img: "https://images-na.ssl-images-amazon.com/images/I/31eZfz2aiGL.jpg",
                    count: 1,
                    price: 40,
                    name: "Beach Ball"
                }
            ]
        };
    }

    _incrementCount = index => {
        this.setState(prevState => {
            const { images } = prevState;
            images[index].count = ++images[index].count;
            return { images, refresh: !prevState.refresh };
        });
    };

    _decrementCount = index => {
        this.setState(prevState => {
            const { images } = prevState;
            images[index].count = --images[index].count;
            return { images, refresh: !prevState.refresh };
        });
    };

    _flatListHighlight = index => {
        this.setState({ click: true });
    };

    itemPressed = async item => {
        console.log(item)
        console.log("This is the state", this.state.selected);
        this.setState({ selected: item.time })
    };

    _renderItem = (item, index) => (
        <View style={{ flex: 1 }}>
            {console.log("time=", item.item)}
            {console.log("sel=", this.state.selected)}
            <Duration
                selected={item.item.time == this.state.selected}
                key={item.id}
                item={item.item}
                onSelectItem={this.itemPressed.bind(this)}
            />
        </View>
    );

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={{ flexDirection: "row", height: 90, alignItems: "flex-end" }} >
                        <Left>
                            {/* <View style={styles.countStyle}> */}
                                <Icon
                                    name="arrow-back"
                                    style={{ color: "white", fontSize: 22, marginTop: 30, marginLeft: 20 }}
                                />
                            {/* </View> */}
                        </Left>

                        <Body style={{ marginRight: 180, marginTop: 30 }}>
                            <Title style={styles.header}>CART</Title>
                        </Body>
                        <Right style={{ flex: 1, flexDirection: "row", alignItems: "baseline", marginTop: 30 }}>
                            <Icon
                                name="search"
                                style={{ color: "white", marginRight: 15, fontSize: 22 }} />
                            <Icon
                                name="cart"
                                style={{ color: "white", fontSize: 22 }} />

                        </Right>
                    </LinearGradient>
                </View>
                <View style={styles.container}>
                    <FlatList
                        data={this.state.images}
                        extraData={this.state.refresh}
                        keyExtractor={(item, index) => `${item.img}${index}`}
                        renderItem={({ item, index }) => (
                            <View style={{ flex: 1, paddingLeft: 30, paddingTop: 15 }}>
                                <Card style={{ height: 120, width: "90%", borderRadius: 13 }}>
                                    <CardItem style={{ borderRadius: 13 }}>
                                        <View>
                                            <Image
                                                style={{ height: 90, width: 90, margin: 10 }}
                                                source={{ uri: item.img }}
                                            />
                                        </View>
                                        <View style={{ flex: 1, flexDirection: "column" }}>
                                            <View style={{ flex: 1, marginTop: 10 }}>
                                                <Text style={styles.item}> {item.name}</Text>
                                            </View>

                                            <View style={styles.countStyle}>
                                                <Icon
                                                    name="remove"
                                                    style={{ color: "black", fontSize: 17 }}
                                                    onPress={() => this._decrementCount(index)}
                                                />
                                                <Text
                                                    style={{
                                                        fontSize: 15,
                                                        marginLeft: 8,
                                                        marginRight: 8
                                                    }}
                                                >
                                                    {item.count}
                                                </Text>

                                                <Icon
                                                    name="ios-add"
                                                    style={{ color: "black", fontSize: 17 }}
                                                    onPress={() => this._incrementCount(index)}
                                                />
                                            </View>
                                            <View style={{ flex: 1, marginTop: 6 }}>
                                                <Text style={{ fontSize: 15 }}>
                                                    ${item.price * item.count}{" "}
                                                </Text>
                                            </View>
                                        </View>
                                        <View>
                                            <Icon
                                                name="trash"
                                                size={18}
                                                color="black"
                                                style={{ marginLeft: 40 }}
                                            />
                                        </View>
                                    </CardItem>
                                </Card>
                            </View>
                        )}
                    />
                </View>

                <View style={styles.container}>
                    <View style={{ margin: 15 }}>
                        <Text style={{ fontSize: 20 }}> Select Duration</Text>
                    </View>
                    <View style={{ flex: 1 }}>
                        <FlatList
                            data={this.state.duration}
                            extraData={this.state}
                            numColumns={3}
                            keyExtractor={(item, index) => index}
                            renderItem={this._renderItem}
                        />
                    </View>
                    <View style={{ marginTop: 30 }}>
                        <TouchableOpacity
                            onPress={() => {
                                alert("Order Placed");
                            }}

                        >
                            <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.orderButton} >
                                <Text style={styles.buttonText}>
                                    PLACE ORDER
                    </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        );
    }
}
const styles = StyleSheet.create({
    header: {
        //fontSize:60,
        //marginRight:100,
        // alignItems: "center",
        fontSize: 20,
        color: "white",
        // justifyContent: "center"
    },
    item: {
        fontSize: 16
    },
    countStyle: {
        flex: 1,
        flexDirection: "row",
        marginTop: 10,
        // backgroundColor: "yellow",
        padding: 10,
        alignItems: "center"
        // justifyContent:"center"
    },
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "#e5e5e5"
    },
    orderButton: {
        backgroundColor: "#00b280",
        alignItems: "center",
        justifyContent: "center",
        width: 230,
        marginHorizontal: 70,
        marginBottom: 30,
        height: 45,
        borderRadius: 40
    },
    flatDuration: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center",
        // backgroundColor: "yellow"
    },
    backgroundDuration: {
        flex: 1,
        flexDirection: "column",
        alignItems: "center"
    },
    linearGradient: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 40
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: 'white',

    },
    flex: {
        flex: 1
    },
    button: {
        borderColor: "#000066",
        // borderWidth: 1,
        // borderRadius: 10
    },
    buttonPress: {

        borderColor: "#000066",
        backgroundColor: "#000066",
        // borderWidth: 1,
        // borderRadius: 10
    }
})
export default Cart;






