// import React, { Component } from "react"
// import _ from 'lodash';
// import services from "../services";
// import { Card, Form, Image, Header, Button, Modal } from "semantic-ui-react"
// import { FileUpload } from "primereact/fileupload";
// import Placeholder from "../components/MainLayout/Placeholder.png"
// import Moment from 'react-moment';
// import CustomModal from "../components/CustomModal/CustomModal";
// export default class Editorials extends Component {
//   state = {
//     editorial: [],
//     updateEditorial: [],
//     modalOpen: false,
//     addEditorial: [],
//     title: "",
//     description: "",
//     image: "",
//     _id: ""
//   }

//   refreshList = () => {
//     this.getEditorials();
//   };

//   addEditorials = async () => {
//     const payload = {
//       title: this.state.title,
//       description: this.state.description,
//       mediaType: "image",
//       media: { media: "https://s3.ap-south-1.amazonaws.com/hoadajme/insta/1.jpg", thumb: "https://s3.ap-south-1.amazonaws.com/hoadajme/insta/1.jpg", type: "image" },
//     };

//     console.log("This is my payload data", payload);
//     try {
//       const data = await services.auth.addEditorial(payload);
//       console.log("This is ad editotal", data)
//       this.setState({
//         modalOpen: false
//       }, () => { this.getEditorials() })

//     } catch (error) {
//       console.log(error);
//     }
//   };

//   componentDidMount() {
//     this.getEditorials();
//   }

//   async getEditorials() {
//     try {
//       const { data } = await services.auth.getEditorials();
//       console.log("Editorial Data", data)
//       this.setState({ editorial: data })
//     }
//     catch (error) {
//       console.log(error)
//     }
//   }

//   updateEditorials = async (record) => {

//     console.log("This is the record", record);
//     const payload = {
//       title: record.title,
//       description: record.description,
//       _id: this.state.selectedEditorial._id,
//       mediaType: "image",
//       media: { media: record.media && record.media.image, thumb:  record.media && record.media.image, type: "image" },
//     };

//     console.log("This is my payload data update", payload);
//     try {
//       const { data } = await services.auth.updateEditorials(payload);
//       console.log("Update Editorial Data", data)
//       this.setState(
//         {
//           selectedEditorial: false
//         }
//         , () => { this.getEditorials() }
//       )
//     }
//     catch (error) {
//       console.log(error)
//     }
//   }

//   selectEditorial = (item) => this.setState({ selectedEditorial: item })

//   closeUpdateModal = () => {

//     this.setState({ selectedEditorial: {}, modalOpen: false })
//   }

//   handleOpen = () => this.setState({ modalOpen: true })

//   handleClose = () => this.setState({ modalOpen: false })

//   render() {
//     return (
//       <div>
//         <Header as="h1">
//           Editorials
//           <CustomModal
//             show={!_.isEmpty(this.state.selectedEditorial)}
//             handleClose={this.closeUpdateModal}
//             selectedEditorial={this.state.selectedEditorial}
//             updateEditorials={this.updateEditorials}
//           />
//           <Modal
//             onClose={this.handleClose}
//             open={this.state.modalOpen}
//             style={{ margin: "28%", width: "100vh" }}
//             // style={{ width: "100vh" }}
//             trigger={
//               <Button
//                 icon="add"
//                 content=" Editorial"
//                 floated="right"
//                 color='grey'
//                 onClick={this.handleOpen}
//               />
//             }
//             size="small"
//             closeIcon
//           >
//             <Modal.Header as="h4">
//               Add Editorial
//             </Modal.Header>
//             <Modal.Content>
//               <Form>
//                 <Form.Field   >
//                   <label><strong>Title</strong></label>
//                   <input
//                     placeholder='Enter Title'
//                     onChange={(e) => { this.setState({ title: e.target.value }) }}
//                   />
//                 </Form.Field>

//                 <Form.TextArea
//                   label="Description"
//                   placeholder="Enter Description"
//                   onChange={(e) => { this.setState({ description: e.target.value }) }}
//                 />
//                 <Form.Field style={{
//                   display: "flex",
//                   flexDirection: "row",
//                   padding: 10
//                 }}>
//                   <label style={{ margin: 5 }}>
//                     <strong>Cover Image</strong>
//                   </label>
//                   <Image
//                     src={Placeholder}
//                     style={{
//                       height: "25vh",
//                       width: "25vh",
//                       margin: 10
//                     }}
//                   />
//                   <div style={{ marginTop: 40 }}>
//                     <FileUpload
//                       url="./upload"
//                       mode="basic"
//                       chooseLabel=""
//                     />
//                   </div>
//                 </Form.Field>
//               </Form>
//             </Modal.Content>
//             <Modal.Actions>
//               <Button
//                 style={{ margin: 10 }}
//                 content="Cancel"
//                 floated="right"
//                 color='grey'
//                 onClick={this.handleClose}
//               />
//               <Button
//                 style={{ margin: 10 }}
//                 content="Add"
//                 floated="right"
//                 color='grey'
//                 onClick={this.addEditorials}
//               />
//             </Modal.Actions>
//           </Modal>
//         </Header>
//         <Card.Group itemsPerRow={4}>
//           {
//             (this.state.editorial.map((item, index) => {
//               return (
//                 <div style={{ margin: 20 }} key={index}>
//                   <Card
//                     color="teal"
//                     style={{ margin: 30, width: "40vh" }}
//                     onClick={() => this.selectEditorial(item)}
//                   >
//                     <Card.Header style={{ margin: 5 }}>
//                       {item.title}
//                     </Card.Header>
//                     <Image src={item.media && item.media.thumb}
//                       style={{
//                         height: "35vh",
//                         width: "35vh",
//                         margin: 15
//                       }}
//                     />
//                     <Card.Content>
//                       <Moment format="MMM-DD-YYYY" style={{ margin: 5 }}>
//                         {item.createdAt}
//                       </Moment>
//                     </Card.Content>
//                   </Card>
//                 </div>
//               );
//             }
//             ))
//           }
//         </Card.Group>
//       </div>
//     );
//   }
// }






























