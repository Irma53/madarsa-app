import React from "react";
import { View, Text } from "native-base";
import {
  StyleSheet
} from "react-native"

class Splash extends React.Component {

  render() {
    return (
      <View style={styles.main}>
        <Text style={styles.header}>Splash Screen</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  header: {
    //marginTop: 200,
    fontSize: 20,

  },
  main: {
    backgroundColor: "skyblue",
    alignItems: "center",
    flex: 1,
    justifyContent: "center"

  }
})


export default Splash;
