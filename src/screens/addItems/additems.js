import React from "react";
import {
  Container,
  View,
  Text,
  Item,
  Input,
  Left,
  Icon,
  Right,
  Button,
  Header,
  Body,
  Title
} from "native-base";

import {
  StyleSheet,
  WebView,
  Platform,
  Linking,
  TouchableOpacity
} from "react-native";
import DateTimePicker from "react-native-modal-datetime-picker";
import {
  titleConstraint,
  descriptionConstraint,
  durationConstraint,
  urlConstraint
} from "../../utils/constraints";
import Snackbar from "react-native-snackbar";

// import TimePicker from "react-native-navybits-date-time-picker";
import moment from "moment";
var validate = require("validate.js");

// moment.HTML5_FMT.TIME_SECONDS;

class AddItems extends React.Component {
  static navigationOptions = {
    header: null
  };
  constructor() {
    super();
    this.state = {
      isTimePickerVisible: false,
      chosenTime: "",
      title: "",
      description: "",
      duration: "",
      url: ""
      // disabled: true,
      // selectedDate: false,
      // accentColor: "",
      // cancelColor: "",
      // okColor: ""
    };
  }

  _Submit = async () => {
    var constraints = {
      Title: titleConstraint,
      Description: descriptionConstraint,
      Duration: durationConstraint,
      URL: urlConstraint
    };
    var validation = validate(
      {
        Title: this.state.title,
        Description: this.state.description,
        Duration: this.state.duration,
        URL: this.state.url
      },
      constraints,
      { format: "flat" }
    );
    //   console.log(validation);
    if (validation) {
      Snackbar.show({
        title: validation[0],
        duration: Snackbar.LENGTH_LONG
      });
      return;
    }
  };

  _showTimePicker = () => this.setState({ isTimePickerVisible: true });

  _hideTimePicker = time =>
    this.setState({
      isTimePickerVisible: false

      //'MMMM, Do YYYY HH:mm'
    });

  _handleTimePicked = time => {
    this.setState({
      isTimePickerVisible: false,
      chosenTime: moment(time).format("HH:mm:ss")
    });
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
      <Header
          style={{ backgroundColor: "white" }}
          androidStatusBarColor="white"
        >
          <Left>
            <Icon
              name="arrow-back"
              color="black"
              onPress={() => this.props.navigation.navigate("HomeScreen")}
            />
          </Left>
            <Body>
            <Title style={styles.body}>Add Item</Title>
          </Body> 
        </Header>
      <View style={styles.main}>
        <View style={styles.bottomcontainer}>
          <Item regular style={{ height: 45 }}>
            <Input
              placeholder={"Title"}
              placeholderTextColor={"black"}
              onChangeText={text => this.setState({ title: text })}
            />
          </Item>
          <Item regular style={styles.describe}>
            <Input
              placeholder={"Description"}
              placeholderTextColor={"black"}
              onChangeText={text => this.setState({ description: text })}
            />
          </Item>

          <Item regular style={styles.des_input}>
            <TouchableOpacity onPress={this._showTimePicker}>
              <Text
                style={{ marginTop: 20, marginLeft: 8 }}
                onChangeText={text => this.setState({ duration: text })}
              >
                {this.state.chosenTime
                  ? this.state.chosenTime
                  : "Time Duration"}
              </Text>
            </TouchableOpacity>

            <DateTimePicker
              isVisible={this.state.isTimePickerVisible}
              onConfirm={this._handleTimePicked}
              onCancel={this._hideTimePicker}
              mode="time"
              datePickerModeAndroid="spinner"
              enableSeconds={true}
            />

            {/* <TimePicker
                // accentColor={this.state.accentColor}
                // okColor={this.state.okColor}
                // cancelColor={this.state.cancelColor}
                 isVisible={this.state.isTimePickerVisible}
                // okText={"OK"}
                // cancelText={"CANCEL"}
                 mode="time"
                datePickerModeAndroid="spinner"
                // disabled={this.state.disabled}
                onConfirm={this._handleTimePicked}
                onCancel={this._hideTimePicker}
                enableSeconds={true}
               // date={this.state.isTimePickerVisible}
                ref="TimePicker"
              /> */}
          </Item>

          <Item regular style={{ marginTop: 20, height: 45 }}>
            <Input
              placeholder={"Youtube URL ..."}
              placeholderTextColor={"black"}
              onChangeText={text => this.setState({ url: text })}
            />
          </Item>

          <View
            style={{
              alignSelf: "stretch"
            }}
          >
            <TouchableOpacity style={styles.button} onPress={this._Submit}>
              <Text style={styles.buttonText}>Submit</Text>
              
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={styles.ybutton}
              onPress={() =>
                Linking.openURL("https://www.youtube.com/watch?v=pEEOThZLEVs")
              }
            >
              <Icon active name="logo-youtube" size={20} style={styles.icons} />
              <Text style={styles.ybuttonText} uppercase={true}>
                Youtube
              </Text>
            </TouchableOpacity> */}
          </View>
        </View>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    //marginTop: 200,
    fontSize: 20
  },
  main: {
    backgroundColor: "white",
    // alignItems: "center",
    flex: 1,
    justifyContent: "center"
    //  padding: 70,
    // paddingLeft: 30,
    // paddingRight: 30
  },
  bottomcontainer: {
    flex: 1,
    paddingTop: 25,
    paddingLeft: 30,
    paddingRight: 30
  },
  icons: {
    color: "white",
    marginRight: 10
  },

  input: {
    color: "white"
  },
  des_input: {
    marginTop: 20,
    paddingBottom: 20,
    height: 45
  },
  button: {
    //flex:1,
    marginTop: 80,
    height: 45,
    marginLeft: 50,
    width: 200,
    alignItems: "center",
    borderRadius: 50,
    justifyContent: "center",
    backgroundColor: "black"
  },

  buttonText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16
  },
  ybutton: {
    flexDirection: "row",
    marginTop: 20,
    marginLeft: 80,
    marginBottom: 10,
    width: 150,
    height: 45,
    borderRadius: 50,
    backgroundColor: "red",
    alignItems: "center",
    justifyContent: "center"
  },

  ybuttonText: {
    color: "white",
    fontWeight: "bold",
    fontSize: 16
  },
  describe: {
    paddingBottom: 30,
    marginTop: 20,
    height: 80
  },
  body:{
  
    // alignItems: "center",
    fontSize: 20,
    color: "black",
    marginLeft: 10,
    justifyContent: "center"
  
}
});

export default AddItems;
