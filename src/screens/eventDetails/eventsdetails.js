import React from "react";
import { StyleSheet, TouchableOpacity, FlatList, Image, ImageBackground } from "react-native";
import {
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
    Button,
    Text,
} from "native-base";
import LinearGradient from "react-native-linear-gradient";
import Moment from 'react-moment';

class EventsDetails extends React.Component {

    static navigationOptions = {
        header: null
    };

    render() {
        const item = this.props.navigation.state.params.info
        console.log("item data", item)
        console.log("image data", item.image)
        return (
            <View style={{ flex: 1 }}>
                {/* <ImageBackground source={{ uri: "https://image.freepik.com/free-vector/beach-tree-palms_18591-722.jpg" }} style={{ width: '100%', height: '100%' }}> */}
                <View >
                    <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.gradeint} >
                        <Left>
                            <Button onPress={() => this.props.navigation.pop()} transparent>
                                <Icon name="ios-arrow-back" style={styles.backArrow} />
                            </Button>
                            {/* <Icon
                                name="arrow-back"
                                style={styles.backArrow}
                            /> */}
                        </Left>
                        <Body style={{ marginRight: 30, marginTop: 30 }}>
                            <Title style={styles.header}>Event Details</Title>
                        </Body>
                        <Right style={styles.headerRight}>
                            <Icon
                                name="search"
                                style={[styles.headerRightIcons, { marginLeft: 55 }]} />
                            <Icon
                                name="cart"
                                style={[styles.headerRightIcons, { margin: 15 }]}
                            />
                        </Right>
                    </LinearGradient>
                </View>
                <Image
                    style={styles.imageStyle}
                    source={{ uri: item.image }}
                />
                <View style={{ flex: 1, backgroundColor: "white" }}>
                    <View style={{ flex: 1, margin: 10 }}>
                        <Text style={{ fontSize: 17, fontWeight: "bold", marginHorizontal: 10 }}>
                            {item.title}
                        </Text>
                        <View style={{ margin: 10 }}>
                            <Text style={{ fontSize: 14, fontWeight: "bold" }}>
                                {/* <Moment format="MMM-DD-YYYY H:m A"> */}
                                {item.date_time}
                                {/* </Moment> */}

                            </Text>
                            <Text style={{ color: "grey", fontSize: 13, margin: 5 }}>
                                {item.description}

                            </Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, margin: 10, marginBottom: 30 }}>
                        <Text style={{ fontSize: 17, fontWeight: "bold", marginHorizontal: 10 }}>
                            LOCATION
                    </Text>
                        <View style={{ margin: 10 }}>
                            <Text style={{ fontSize: 14, color: "grey" }}>
                                {item.location}
                            </Text>
                            {/* <Text style={{ color: "grey", fontSize: 13 }}>
                                24255 Giraffe Hill Drive, {"\n"}Grand Prairie, Texas.
                       </Text> */}
                        </View>
                    </View>
                </View>
            </View>

        )
    }
}


const styles = StyleSheet.create({
    header: {
        width: 190,
        fontSize: 16,
        color: "white",
        marginLeft: 20,
        marginRight: 70,
    },
    countStyle: {
        flex: 1,
        flexDirection: "row",
        marginTop: 10,
        padding: 6,
        alignItems: "center"
    },
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: "white"
    },
    bottomContainer: {
        backgroundColor: "white"
    },
    orderButton: {
        alignItems: "center",
        justifyContent: "center",
        marginHorizontal: 40,
        marginBottom: 20,
        marginTop: 10,
        borderRadius: 40,
        borderColor: 'black',
        width: 280,
        shadowColor: 'black',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 1,
        shadowRadius: 4,
        elevation: 5,
    },
    item: {
        flex: 1,
        fontSize: 14,
        // marginTop: 20,
        fontWeight: "bold"
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        margin: 10,
        color: 'white',
    },
    upperContainer: {
        justifyContent: "center",
        alignItems: "center",
        flex: 1,
        padding: 5,
        backgroundColor: "white",
    },
    gradeint: {
        flexDirection: "row",
        height: 70,
        alignItems: "flex-end"
    },
    backArrow: {
        color: "white",
        fontSize: 22,
        marginTop: 30,
        marginLeft: 20
    },
    headerRight: {
        flex: 1,
        flexDirection: "row",
        alignItems: "baseline",
        marginTop: 30
    },
    headerRightIcons: {
        color: "white",
        fontSize: 22,
    },
    upperItem: {
        height: 40,
        backgroundColor: "white"
    },
    items: {
        height: 40,
        backgroundColor: "white",
        marginTop: 10
    },
    itemText: {
        flex: 1,
        color: "gray",
        fontSize: 13,
        marginLeft: 7
    },
    itemData: {
        color: "lightgray",
        fontSize: 12
    },
    reviewOrder: {
        fontSize: 20,
        fontWeight: "bold",
        paddingLeft: 20,
        marginBottom: 8
    },
    card: {
        height: 80,
        width: "92%",
        borderRadius: 13,
        // backgroundColor:"lightgray"
    },
    img: {
        height: 50,
        width: 50,
        margin: 15
    },
    countData: {
        fontSize: 15,
        color: "black",
        marginHorizontal: 20,
        fontWeight: "bold"
    },
    imageStyle: {
        width: "100%",
        height: "35%",
    },
    imageCard: {
        backgroundColor: "#3FE0D0",
        flexDirection: "row",
        marginLeft: 17,
        marginRight: 17,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10
    },
    imageCardTxt: {
        fontSize: 18,
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    secondCardTxt: {
        fontSize: 13,
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    }


})
export default EventsDetails;






