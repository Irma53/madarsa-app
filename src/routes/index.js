import {
  createStackNavigator,
  createMaterialTopTabNavigator
} from "react-navigation";
import Splash from "../screens/splash"
import HomeScreen from "../screens/homeScreen";
import ViewItems from "../screens/viewItems"
import AddItems from "./src/screens/addItems";
import WatchVideos from "./src/screens/watchVideos";
import Cart from './src/screens/cartScreen';
import Test from './src/screens/test';
import Notes from './src/screens/example';
import Order from './src/screens/orderConfirmation';
import SpikeBall from './src/screens/spikeBall'
import PackagesPlan from './src/screens/packagesPlan'
import SubscriptionDetail from './src/screens/Subscription'
import SubscriptionsPlan from './src/screens/subscriptionPlan'
import EventsDetails from './src/screens/eventDetails'
import Events from './src/screens/Events'

export const RootStack = createStackNavigator(
  {
    splashScreen: {
      screen: Splash
    },

    Home: {
      screen: HomeScreen
    },
    view: {
      screen: ViewItems
    },
    Add: {
      screen: AddItems
    },
    Watch: {
      screen: WatchVideos
    },
    Cart: {
      screen: Cart
    },
    Order: {
      screen: Order
    },
    spikeBall:{
      screen : SpikeBall
  },
  Packages:{
    screen :PackagesPlan
},
subscriptiondetails:{
  screen : SubscriptionDetail
},
subscriptionPlan:{
  screen : SubscriptionsPlan
},
Events:{
  screen :Events
},
eventDetails:{
  screen : EventsDetails
}

  },
  {
    headerMode: "none"
  }
);

export default RootStack;