import firebase from "react-native-firebase";
import { Platform } from "react-native";
import Api from "./api";
// import { updateDeviceToken } from "../store/actions/auth.actions";

class NotificationManager {
  start(notificationListener) {
  
    // firebase
    //   .auth()
    //   .signInAnonymouslyAndRetrieveData()
    //   .then(user => {
    //     console.log("user====>", user.isAnonymous);
    //   });

    firebase
      .messaging()
      .hasPermission()
      .then(() => {
        if (enabled) {
          console.log("User has permission");
        } else {
          console.log("user doesn't have permission");
        }
      })
      .catch(() => {}); //console.warn("notification permission rejected"));

    firebase
      .messaging()
      .getToken()
      .then(fcmToken => {
        console.log("fcmToken===>", fcmToken);
        if (fcmToken) {
          const payload = { device_token: fcmToken };

          console.log("payload===>", payload);
          Api.saveDeviceToken(payload)
            .then(response =>
              console.log("Device token saved", response.response.message)
            )
            .catch(e => console.log(e));
        } else {
          console.warn("user doesn't have a device token yet");
        }
      })
      .catch(e => {
        console.warn(e);
      });

    // firebase.notifications().onNotificationDisplayed(notification => {
    //   console.log("notification==>", notification);
    // //   store.dispatch(fetchNewsfeed(false, true));
    // //   store.dispatch(twimCountersLiveUpdate(notification._data));
    // });

    firebase.notifications().onNotification(notification => {
      console.log("notification heloooo =>>", notification);
      console.log("notification data =>>", notification._data);
      if (notificationListener) {
        notificationListener(notification._data);
      }
      
    });

    firebase
      .notifications()
      .getInitialNotification()
      .then(notificationOpen => {
        console.log("getInitialNotification =>>", notificationOpen);
        if (notificationOpen) {
          // App was opened by a notification
          // Get the action triggered by the notification being opened
          const action = notificationOpen.action;
          // Get information about the notification that was opened
          const notification = notificationOpen.notification;
        }
      });

    firebase.notifications().onNotificationOpened(notif => {
      console.log("onNotificationOpened =>>", notif);
        if (notificationListener) {
          notificationListener(notif.notification._data);
        }
    });
  }
}

export default new NotificationManager();
