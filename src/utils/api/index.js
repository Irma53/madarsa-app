// import store from "../../store/index";
import Store from "../store";
import Loader from "../Loader";

class Api {
  constructor() {
    this.baseUrl = "http://142.93.188.94/Clients/beach_bandits/api/";
  }

  _checkStatus(response) {
    console.log("post=>>", this.loading);
    if (this.loading !== undefined) {
      Loader.hide(this.loading);
      this.loading = undefined;
      console.log(response);
      console.log(response._bodyText);
    }

    if (response.status >= 200 && response.status < 600) {
      return response;
    } else {
      var error = new Error("An unknown error occured, please try again");
      if (response) {
        try {
          error = new Error(JSON.parse(response._bodyText).Response);
        } catch (e) {
          error = new Error(response._bodyText);
        }
      }
      error.response = response;
      throw error;
    }
  }

  _request(url, method, params) {
    console.log("Pre=>>", this.loading);
    if (this.loading == undefined) {
      this.loading = Loader.show();
    }

    var fetchParams = {
      method: method
    };

    Object.assign(fetchParams, params || {});
    fetchParams.headers = Object.assign({}, fetchParams.headers || {}, {
      Accept: "application/json",
      "Content-Type": "application/json"
    });

    if (!fetchParams.ignoreToken) {
      fetchParams.headers["Authorization"] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6InJvN1NEWGJDTDljTnRzNVAifQ.eyJpc3MiOiJodHRwOlwvXC8xNDIuOTMuMTg4Ljk0XC9DbGllbnRzXC9iZWFjaF9iYW5kaXRzIiwic3ViIjoiNiIsImp0aSI6InJvN1NEWGJDTDljTnRzNVAiLCJpYXQiOjE1NTEyNjc3MTksIm5iZiI6MTU1MTI2NzcxOSwiZXhwIjoxNTUxMjc0OTE5LCJybGkiOjE1NTE3MDY5MTl9.hexiwd7hYnFU19LfPykGWWgC5oCiTCmNoR8TTeskmm8";
    } else {
      delete fetchParams.ignoreToken;
    }

    url = this.baseUrl + url;
    console.log(fetchParams);
    return new Promise(
      function (resolve, reject) {
        fetch(url, fetchParams)
          .then(this._checkStatus.bind(this))
          .then(response => response.json())
          .then(response => {
            console.log(typeof response);
            // if (response === ["Token Not Valid"]) {
            //   this.renewToken();
            // }
            console.log("Resolving ==> ", response);
            resolve(response);
          })
          .catch(error => {
            console.log(error);
            reject(error);
          });
      }.bind(this)
    );
  }

  _requestImage(url, method, params) {
    var fetchParams = {
      method: method
    };

    Object.assign(fetchParams, params || {});
    fetchParams.headers = Object.assign({}, fetchParams.headers || {}, {
      Accept: "application/json",
      "Content-Type": "multipart/form-data"

    });
    if (!fetchParams.ignoreToken) {
      fetchParams.headers["Authorization"] = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImp0aSI6InJvN1NEWGJDTDljTnRzNVAifQ.eyJpc3MiOiJodHRwOlwvXC8xNDIuOTMuMTg4Ljk0XC9DbGllbnRzXC9iZWFjaF9iYW5kaXRzIiwic3ViIjoiNiIsImp0aSI6InJvN1NEWGJDTDljTnRzNVAiLCJpYXQiOjE1NTEyNjc3MTksIm5iZiI6MTU1MTI2NzcxOSwiZXhwIjoxNTUxMjc0OTE5LCJybGkiOjE1NTE3MDY5MTl9.hexiwd7hYnFU19LfPykGWWgC5oCiTCmNoR8TTeskmm8";
    } else {
      delete fetchParams.ignoreToken;
    }

    url = this.baseUrl + url;
    return new Promise(
      function (resolve, reject) {
        fetch(url, fetchParams)
          .then(this._checkStatus)
          .then(response => response.json())
          .then(response => resolve(response))
          .catch(error => reject(error));
      }.bind(this)
    );
  }

  _get(url, params) {
    let index = 0;
    for (var key in params) {
      url =
        index == 0
          ? url + "?" + key + "=" + params[key]
          : url + "&" + key + "=" + params[key];
      index++;
    }
    console.log("url === >", url);
    return this._request(url, "GET", params);
  }

  _post(url, params) {
    params.body = JSON.stringify(params.body);
    return this._request(url, "POST", params);
  }

  _postImage(url, params) {
    params.body = params.body;
    return this._requestImage(url, "POST", params);
  }

  _put(url, params) {
    params.body = JSON.stringify(params.body);
    return this._request(url, "PUT", params);
  }

  _putFile(url, params) {
    params.body = params.body;
    return this._requestFile(url, "PUT", params);
  }

  _delete(url, params) {
    params.body = JSON.stringify(params.body);
    return this._request(url, "DELETE", params);
  }

  getFBUserInfo(accesstoken) {
    return fetch(
      `https://graph.facebook.com/me?fields=id,name,email,picture&access_token=${accesstoken}`
    ).then(response => response.json());
  }

  userFbSignUp(user) {
    return this._post("users/facebookLogin", { body: user, ignoreToken: true });
  }

  login(data) {
    return this._post("login", { body: data, ignoreToken: true });
  }

  logout() {
    return this._post("logout", {});
  }

  signup(data) {
    return this._post("register", { body: data, ignoreToken: true });
  }

  renewToken() {
    return this._get("renew-token", {});
  }

  updateProfile(data) {
    return this._post("edit_profile", { body: data });
  }

  changePassword(data) {
    return this._post("change_password", { body: data });
  }

  forgotPassword(data) {
    return this._post("password-reset-email", {
      body: data,
      ignoreToken: true
    });
  }

  resetPassword(data) {
    return this._post("reset-password", { body: data, ignoreToken: true });
  }

  updateNotificationSettings(data) {
    return this._post("edit_notification_setting", { body: data });
  }

  saveNotes(data) {
    return this._post("add_card_notes", { body: data });
  }

  saveHand(data) {
    return this._post("add_save_hand", { body: data });
  }

  updateHand(data) {
    return this._post("edit_save_hand", { body: data });
  }

  deleteHand(data) {
    return this._post("delete_save_hand", { body: data });
  }

  authorizeStripe(data) {
    return this._post("authorize_stripe", { body: data });
  }

  addPayment(data) {
    return this._post("add-payment", { body: data });
  }
  addOrderPayment(data) {
    return this._post("add-order-payment", { body: data });
  }

  redeemFreeDeck(data) {
    return this._post("free_deck_promo", { body: data });
  }

  processPayment(data) {
    return this._post("post-braintree-payment", { body: data });
  }

  getCategories() {
    return this._get("item-categories", {});
  }

  getBraintreeToken() {
    return this._get("get-braintree-token", {});
  }

  getBanners() {
    return this._get("get-banners", {});
  }

  getItemsWithCategory(data) {
    return this._get("items", data);
  }

  searchItems(data) {
    return this._get("search-items", data);
  }

  getSubscriptionPlans() {
    return this._get("subscription-plans", {});
  }

  getUserCurrentSubscription(data) {
    return this._get("current-subscription", data);
  }

  getPlanById(data) {
    return this._get("subscription-plan", data);
  }

  getPurchasedCards() {
    return this._get("get_purchase_and_gift_decks", {});
  }

  getPackages() {
    return this._get("packages", {});
  }

  getPackagesById() {
    return this._get("package?package_id=2");
  }


  getCart() {
    return this._get("cart-items", {});
  }

  getEvents() {
    return this._get("get-events", {});
  }

  addToCart(data) {
    return this._post("add-to-cart", { body: data });
  }

  deleteFromCart(data) {
    return this._delete("delete-item", { body: data });
  }

  placeOrder(data) {
    return this._post("add-order", { body: data });
  }

  getOrders(data) {
    return this._get("orders", data);
  }

  saveDeviceToken(data) {
    return this._post("save_device_token", { body: data });
  }

  contactUs(data) {
    return this._post("contact_us", { body: data });
  }
}

let singleton = new Api();

export default singleton;
