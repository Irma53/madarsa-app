/**
 * Created by basit on 27/12/2015.
 */
"use strict";

import React, { AsyncStorage } from "react-native";

const EventEmitter = require("EventEmitter");

const USER_KEY = "beach-user";
const TOKEN_KEY = "beach-user-token";
const CART_KEY = "beach-cart";

class Store extends EventEmitter {
  constructor() {
    super();
    this.state = { cart: [] };
  }

  getState() {
    return this.state;
  }

  saveUser(user) {
    this.state.user = user;
    this._saveUserObject(user);
  }

  saveToken(token) {
    this.state.token = token;
    this._saveToken(token);
  }

  setHome(home) {
    this.state.home = home;
  }

  setMainNavigation(nav) {
    this.state.nav = nav;
  }

  async _saveUserObject(user) {
    try {
      await AsyncStorage.setItem(USER_KEY, JSON.stringify(user));
      console.log("User saved");
    } catch (error) {
      console.log("Error saving user");
    }
  }

  async _saveToken(token) {
    try {
      await AsyncStorage.setItem(TOKEN_KEY, token);
    } catch (error) {
      console.log("Error saving token");
    }
  }

  saveCart(cart) {
    this.state.cart = cart;
    this._saveCartObject(cart);
  }
  // savePackage(package) {
  //   this.state.package = package;
  //   this._saveCartObject(package);
  // }

  async _saveCartObject(cart) {
    try {
      await AsyncStorage.setItem(CART_KEY, JSON.stringify(cart));
    } catch (error) {
      console.log("Error saving cart", error);
    }
  }

  async getLastSavedUser() {
    var user = await AsyncStorage.getItem(USER_KEY);
    if (user) {
      user = JSON.parse(user);
    }
    this.state.user = user;
    return user;
  }

  async getLastSavedToken() {
    var token = await AsyncStorage.getItem(TOKEN_KEY);
    this.state.token = token;
    return token;
  }

  async getSavedCart() {
    var cart = await AsyncStorage.getItem(CART_KEY);
    this.state.cart = JSON.parse(cart) || [];
    return JSON.parse(cart);
  }

  async removeUser() {
    var user = AsyncStorage.removeItem(USER_KEY);
    return user;
  }

  async removeToken() {
    var user = AsyncStorage.removeItem(TOKEN_KEY);
    return user;
  }

  get user() {
    return this.state.user;
  }

  get cart() {
    return this.state.cart;
  }

  get token() {
    return this.state.token;
  }

  get home() {
    return this.state.home;
  }

  get main() {
    return this.state.nav;
  }

  get package(){
    return this.state.packages;
  
  }
}

// let singleton = new Store();

export default new Store();
