export const titleConstraint = {
  presence: { allowEmpty: false },
  exclusion: {
    message: "Please enter title"
  }
};

export const descriptionConstraint = {
  presence: { allowEmpty: false },
  exclusion: {
    message: "Please enter description"
  }
};

export const durationConstraint = {
  // presence: { allowEmpty: false },
  exclusion: {
    message: "Please enter duration"
  }
};

export const urlConstraint = {
  presence: { allowEmpty: false },
  exclusion: {
    message: "Please enter Video URL"
  }
};
