import React, { Component } from "react";
import {
  View,
  Text,
  Thumbnail,
  Container,
  Button,
  CardItem,
  Card
} from "native-base";
import { TouchableOpacity, StyleSheet, Image, ImageBackground } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { Assets } from "../images";


class Duration extends Component {
  render() {
    console.log("test", this.props.selected);
    return (
      <TouchableOpacity
        onPress={() => this.props.onSelectItem(this.props.item)}
      >
        <View style={{backgroundColor:"transparent",flex:1}}>
         
           
          {this.props.selected  ? 
           
        (  <ImageBackground source={Assets.mainBackground} style={styles.backgroundImage} >
        <View style={{  alignItems:"center", justifyContent:"center" }}>
          <View style={{flex: 1}} >
          <Text
            style={{
              color: "white",
              fontSize: 12,
              marginTop: 20
            }}
          >
            {this.props.item.time}
          </Text>
        </View>
        <View>
          <Text style={{ fontSize: 25, marginBottom:20, color:"white" }}>{this.props.item.cost}</Text>
        </View>
        </View>
      </ImageBackground>
        )
       
               : 
     (  <ImageBackground  style={styles.backgroundImage} >
     <View style={{alignItems:"center", justifyContent:"center"}} >
      <View style={{ flex: 1 }}>
      <Text
        style={{
          color: "skyblue",
          fontSize: 12,
          marginTop: 20
        }}
      >
        {this.props.item.time}
      </Text>
    </View>
    <View>
      <Text style={{ fontSize: 25, marginBottom:20 }}>{this.props.item.cost}</Text>
    </View>
    </View>
 
  </ImageBackground>)}
        
           
         
           
        </View>
      </TouchableOpacity>

      //   <TouchableOpacity
      //     style={{
      //       alignItems: "center",
      //       flexWrap: "wrap"
      //     }}
      //     onPress={() => this.props.itemPressed(this.props.item)}
      //   >
      //     <Text
      //       style={{
      //         flexDirection: "column",
      //         textAlign: "center",
      //         width: 80,
      //         color: Colors.yellow,
      //         marginTop: 5,
      //         fontSize: 12,
      //         flex: 1
      //       }}
      //     >
      //       {/* //  {this.props.item.id} */}
      //       {this.props.item.name}
      //       {/* // {this.props.item.email} */}
      //     </Text>
      //   </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    // backgroundColor: "red"
  },
  orderButton: {
   // backgroundColor: "#00b280",
    alignItems: "center",
    justifyContent: "center",
    width: 230,
    marginHorizontal: 70,
    marginBottom: 30,
    height: 45,
    borderRadius: 40
  },
  
    backgroundImage: {
        
        flex:1,
        // backgroundColor:"red"
        
        // width: undefined,
        // height: undefined,
        // flexDirection: 'column',
        // backgroundColor:'transparent',
        // justifyContent: 'flex-start',
    
    
    }
});

export default Duration;
