import React from "react";
import {
    View,
    Body,
    Title,
    Left,
    Icon,
    Right,
} from "native-base";
import StyleSheet from 'react-native';
import LinearGradient from "react-native-linear-gradient";

 class lineargradient extends React.Component {
  

  render() {
    return (
        <View >
        <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0.8, y: 1 }} colors={['#00ffff', '#33cc33']} style={styles.gradeint} >
            <Left>
                <Icon
                    name="arrow-back"
                    style={styles.backArrow}
                />
            </Left>
            <Body style={{ marginRight: 30, marginTop: 30 }}>
                <Title style={styles.header}>EVENTS</Title>
            </Body>
            <Right style={styles.headerRight}>
                <Icon
                    name="search"
                    style={[styles.headerRightIcons, { marginLeft: 55 }]} />
                <Icon
                    name="cart"
                    style={[styles.headerRightIcons, { margin: 15 }]}
                />
            </Right>
        </LinearGradient>
        </View>
    )
  }
}
const styles = StyleSheet.create({
    gradeint: {
        flexDirection: "row",
        height: 70,
        alignItems: "flex-end"
    },
    backArrow: {
        color: "white",
        fontSize: 22,
        marginTop: 30,
        marginLeft: 20
    },
    headerRight: {
        flex: 1,
        flexDirection: "row",
        alignItems: "baseline",
        marginTop: 30
    },
    header: {
        width: 190,
        fontSize: 16,
        color: "white",
        marginLeft: 20,
        marginRight: 70,
    },
    headerRightIcons: {
        color: "white",
        fontSize: 22,
    }
})
 export default lineargradient;

  



