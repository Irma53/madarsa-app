import Loading from "react-native-loader-overlay";
class Loader {
  show() {
    return Loading.show({
      color: "#10B4DD",
      size: 15,
      overlayColor: "rgba(0,0,0,0.5)",
      closeOnTouch: true,
      loadingType: "Bubbles" // 'Bubbles', 'DoubleBounce', 'Bars', 'Pulse', 'Spinner'
    });
  }

  hide(loader) {
    Loading.hide(loader);
  }
}

export default new Loader();
