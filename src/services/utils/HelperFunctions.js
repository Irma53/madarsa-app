import { Platform } from "react-native";

export const noEmojiKeyboardType = Platform.select({
  android: "email-address",
  ios: "ascii-capable"
});
  