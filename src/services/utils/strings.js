const Strings = {
  name: "Full Name",
  verificationCode: "Enter your verification code",
  email: "Email",
  password: "Enter new password",
  confirmPassword: "Confirm new password",
  login: "Log In",
  signup: "Create Account",
  forgotPassword: "Forgot Password"
};

export default Strings;
