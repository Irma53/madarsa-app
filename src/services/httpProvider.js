// import axios from "axios";
// // import { getToken } from "../utils/auth.util";
// // import Loader from "../components/loader";

// const BASE_URL = `http://142.93.188.94/Clients/beach_bandits/api/`;


// export async function GetApiRequestHeader(token) {
//     console.log("request header token==>", token);
//     const authToken = token || (await getToken());
//     console.log("This is a auth token", authToken);
//     return {
//       Accept: "application/json",
//       "Content-Type": "application/json",
//       "x-access-token": authToken,
//       Authorization: `Bearer ${authToken}`
//     };
//     if (authToken) {
//       this.updateHeaders(authToken);
//     }
//   }

// const instance = axios.create({
//     baseURL: `${BASE_URL}`,
//     timeout: 60000,
//     withCredentials: true,
//     headers: GetApiRequestHeader()
//   });

//   export async function updateHeaders(token) {
//     // Alter defaults after instance has been created
//     console.log("Update Header ==>", token);
//     const header = await GetApiRequestHeader(token);
//     console.log("This is header", header);
//     instance.defaults.headers = header;
//   }
  

//   export async function request({ method, url, data, headers }) {
//     if (this.loading == undefined) {
//       this.loading = Loader.show();
//     }
//     console.log("method", method);
//     console.log("url", url);
//     console.log("data", data);
//     console.log("header", headers);
//     console.log(`Sending ${method} request to ${BASE_URL}`, url);
  
//     var promise = instance[method](url, data);
//     // console.log("Promise", promise);
  
//     const response = await promise;
  
//     // console.log("request>>>", response);
//     console.log(`Response from ${url}`, response);
//     const payload = response;
//     if (this.loading) {
//       Loader.hide(this.loading);
//       this.loading = undefined;
//     } 
  
//     if (headers) {
//       return {
//         data: payload,
//         headers: response.headers
//       };
//     }
  
//     return payload;
//   }

//   export async function get(url, params, config) {
//     console.log("Categories Url===>", url);
//     return request({ method: "get", url, data: { params }, ...config });
//   }
  
//   export async function del(url, params, config) {
//     return request({ method: "delete", url, data: { params }, ...config });
//   }
  
  
//   export async function post(url, data, config) {
//     console.log("Url", url);
//     console.log("data==>", data);
//     console.log("Config", config);
//     // console.log("config", ...config);
//     return request({ method: "post", url, data, ...config });
//   }
  
//   export async function put(url, data, config) {
//     return request({ method: "put", url, data, ...config });
//   }
  